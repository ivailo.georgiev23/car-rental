import { createRouter, createWebHistory } from "vue-router";
import Dashboard from "../views/Dashboard.vue";
import Vehicles from "@/views/Vehicles/index.vue";
import User from "@/views/User/index.vue";

import AppMain from "@/layout/AppMain.vue";
import BareBones from "@/layout/BareBones.vue";


import { useUserStore } from "@/stores/user";


const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "dashboard",
      component: Dashboard,
      meta: {
        layout: AppMain,
      },
    },
    {
      path: "/vehicles",
      name: "vehicles",
      component: Vehicles,
      meta: {
        layout: AppMain,
      },
    },
    {
      path: "/user",
      name: "user",
      component: User,
      meta: {
        layout: AppMain,
      },
    },
    {
      path: "/login",
      name: "login",
      component: () => import("@/views/Login.vue"),
      meta: {
        layout: BareBones
      }
    },
    {
      path: "/register",
      name: "register",
      component: () => import("@/views/Register.vue"),
      meta: {
        layout: BareBones
      }
    }
  ],
});

router.beforeEach((to, from, next) => {
  const user: { name: string, isAdmin: boolean } = useUserStore();
  const isAuth = user.name ? true : false;

  console.log(isAuth);
  
  if (to.name !== 'login' && !isAuth) {
    next({ name: 'login' })
  }
  else next()
})

export default router;
