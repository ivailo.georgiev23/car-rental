import { acceptHMRUpdate, defineStore } from "pinia";

import { useUserStore } from "./user";

import type { Vehicle } from "@/ts/types";
import type { Booking } from "@/ts/types/Booking";

export const useBookedStore = defineStore({
    id: "booked",
    state: () => ({
        booked: [

        ] as Array<Booking>
    }),
    getters: {
        getBooked: (state) => state.booked,
    },
    actions: {
        book(vehicle: Vehicle | undefined, startDate: string | undefined, endDate: string | undefined) {
            const user = useUserStore()

            this.booked.push({
                username: user.name,
                vehicle,
                startDate,
                endDate
            })

        },
        removeBooking(registrationNumber: string | undefined) {
            console.log(registrationNumber);
            
            this.booked = this.booked.filter(booking => booking.vehicle?.registrationNumber !== registrationNumber);            
        }
    },
})


if (import.meta.hot) {
    import.meta.hot.accept(acceptHMRUpdate(useBookedStore, import.meta.hot))
}
