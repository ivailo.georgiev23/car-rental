import type { Vehicle } from "./Vehicle"

export type Booking = {
    username: string,
    vehicle: Vehicle | undefined,
    startDate: string | undefined,
    endDate: string | undefined
}