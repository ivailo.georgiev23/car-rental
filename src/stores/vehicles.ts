import { acceptHMRUpdate, defineStore } from "pinia";

// import { useUserStore } from "./user";

import type { Vehicle } from "@/ts/types";
import type { Booking } from "@/ts/types/Booking";

export const useVehicleStore = defineStore({
    id: "vehicles",
    state: () => ({
        vehicles: [{
            registrationNumber: 'A0000AA',
            category: 'Small Car',
            make: 'Honda',
            model: 'Civic',
            fuelType: 'Petrol',
            pricePerDay: '25'
        },
        {
            registrationNumber: 'A0001AA',
            category: 'Small Car',
            make: 'Honda',
            model: 'Civic',
            fuelType: 'Petrol',
            pricePerDay: '25'
        },
        {
            registrationNumber: 'A0002AA',
            category: 'Small Car',
            make: 'Honda',
            model: 'Civic',
            fuelType: 'Petrol',
            pricePerDay: '25'
        },
        {
            registrationNumber: 'A0003AA',
            category: 'Estate Car',
            make: 'Honda',
            model: 'Civic',
            fuelType: 'Petrol',
            pricePerDay: '35'
        },
        {
            registrationNumber: 'A0004AA',
            category: 'Estate Car',
            make: 'Honda',
            model: 'Civic',
            fuelType: 'Diesel',
            pricePerDay: '35'
        },
        {
            registrationNumber: 'A0005AA',
            category: 'Van',
            make: 'Honda',
            model: 'Civic',
            fuelType: 'Petrol',
            pricePerDay: '50'
        },
        {
            registrationNumber: 'A0006AA',
            category: 'Van',
            make: 'Honda',
            model: 'Civic',
            fuelType: 'Diesel',
            pricePerDay: '50'
        },
        {
            registrationNumber: 'A0007AA',
            category: 'Small Car',
            make: 'Honda',
            model: 'Civic',
            fuelType: 'Diesel',
            pricePerDay: '25'
        },
        {
            registrationNumber: 'A0008AA',
            category: 'Small Car',
            make: 'Honda',
            model: 'Civic',
            fuelType: 'Petrol',
            pricePerDay: '25'
        }] as Array<Vehicle> | undefined,
        booked: [

        ] as Array<Booking>
    }),
    getters: {
        getAllVehicles: (state) => state.vehicles,
        getAllSmallCars: (state) => state.vehicles?.filter(vehicle => vehicle.category === 'Small Car'),
        getAllEstateCars: (state) => state.vehicles?.filter(vehicle => vehicle.category === 'Small Car'),
        getAllVAns: (state) => state.vehicles?.filter(vehicle => vehicle.category === "Van"),
    },
    actions: {
    },
})


if (import.meta.hot) {
    import.meta.hot.accept(acceptHMRUpdate(useVehicleStore, import.meta.hot))
}
