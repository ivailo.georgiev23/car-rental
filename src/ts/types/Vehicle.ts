export type Vehicle = {
    registrationNumber: string,
    category: 'Small Car' | 'Estate Car' | 'Van',
    make: string,
    model: string,
    fuelType: 'Petrol' | 'Diesel',
    pricePerDay: '25' | '35' | '50'
}