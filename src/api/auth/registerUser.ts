const registerUser = (username: string, password: string, confirmPassword: string): Promise<boolean> => {
    const passwordMatch = password === confirmPassword ? true : false;

    if (username && passwordMatch)
        return Promise.resolve(true);

    return Promise.reject(false)
}

export { registerUser }