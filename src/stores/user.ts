// @ts-check
import { defineStore, acceptHMRUpdate } from 'pinia'

import { authUser } from "@/api";

export const useUserStore = defineStore({
    id: 'user',
    state: () => ({
        name: '',
        isAdmin: false,
    }),
    getters: {
        currentUser: (state) => state
    },
    actions: {
        logout() {

            this.$patch({
                name: '',
                isAdmin: false,
            })
            localStorage.setItem('userToken','');

        },

        /**
         * Attempt to login a user
         * @param {string} user
         * @param {string} password
         */
        async login(user: string, password: string) {

            const userData = await authUser(user, password)

            this.$patch({
                name: user,
                ...userData,
            })

            localStorage.setItem('userToken', JSON.stringify({
                name: user,
                ...userData,
            }));

        },

        checkToken() {
            const userToken = localStorage.getItem('userToken');
            console.log(userToken);
            
            if (userToken) {
                this.$patch(JSON.parse(userToken));
            }
        }
    },
})

if (import.meta.hot) {
    import.meta.hot.accept(acceptHMRUpdate(useUserStore, import.meta.hot))
}
