const authUser = (a: string, p: string): Promise<Object> | void => {
    if (a === 'admin' && p === 'admin') {
        return Promise.resolve({ isAdmin: true })
    } else {
        return Promise.resolve({ isAdmin: false })
    }
    // return Promise.reject(new Error('invalid credentials'))
}

export { authUser }